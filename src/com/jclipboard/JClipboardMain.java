package com.jclipboard;

import com.jclipboard.application.ApplicationController;
import com.jclipboard.listener.GlobalKeyListener;
import com.jclipboard.model.JClipboard;
import com.jclipboard.model.JStringClipboard;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

/**
 * Created by Cinderella Kant on 20.04.14.
 */
public class JClipboardMain {
    static public void main(String[] args) {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }
        JClipboard<String> clipboard = JStringClipboard.getInstance();
        ApplicationController.getInstance().setClipboard(clipboard);
        GlobalScreen.getInstance().addNativeKeyListener(new GlobalKeyListener());
    }
}
