package com.jclipboard.application;

import com.jclipboard.model.JClipboard;

import java.awt.*;
import java.awt.datatransfer.*;

/**
 * Created by Cinderella Kant on 03.05.14.
 */
public class ApplicationController {
    private JClipboard clipboard;
    private static final ApplicationController appContext = new ApplicationController();
    private static final Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    private ApplicationController() {

    }

    public static ApplicationController getInstance() {
        return appContext;
    }

    public JClipboard getClipboard() {
        return clipboard;
    }

    public void setClipboard(JClipboard clipboard) {
        this.clipboard = clipboard;
    }

    public synchronized void processCopyEvent() {
//        Transferable transferable = systemClipboard.getContents(null);
//        String data = null;
//        if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
//            try {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                data = (String) transferable.getTransferData(DataFlavor.stringFlavor);
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            } catch (UnsupportedFlavorException ufe) {
//                ufe.printStackTrace();
//            }
//        }
        //TODO this sleep statement needed for owning the clipboard, fix needed
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard systemClipboard = toolkit.getSystemClipboard();
        try {
            String data = (String) systemClipboard.getData(DataFlavor.stringFlavor);
            System.out.println(data + " copied");
            clipboard.addElement(data);
        } catch (Exception e) {
            System.out.println(e.getMessage() + e.getStackTrace());
        }
    }


    public void processIncreasePositionEvent() {
        if (clipboard.increasePosition()) {
            StringSelection selection = new StringSelection((String) clipboard.getElement());
            systemClipboard.setContents(selection, selection);
        }
    }

    public void processDecreasePositionEvent() {
        if (clipboard.reducePosition()) {
            StringSelection selection = new StringSelection((String) clipboard.getElement());
            systemClipboard.setContents(selection, selection);
        }
    }
}

