package com.jclipboard.enums;

/**
 * Created by Cinderella Kant on 03.05.14.
 */
public enum KeyEvents {
    COPY("Copy event"),
    INCREASE_POSITION("Increase position event"),
    DECREASE_POSITION("Decrease position event");

    private String label;

    KeyEvents(String label) {
        this.label = label;
    }



    @Override
    public String toString() {
        return "KeyEvent{" +
                "label='" + label + '\'' +
                '}';
    }
}
