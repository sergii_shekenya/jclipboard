package com.jclipboard.listener;

/**
 * Created by Cinderella Kant on 19.04.14.
 */

import com.jclipboard.application.ApplicationController;
import com.jclipboard.enums.KeyEvents;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class GlobalKeyListener implements NativeKeyListener {
    public void nativeKeyPressed(NativeKeyEvent e) {
        System.out.println("Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
        System.out.println("Modifiers : " + e.getModifiers() + " : " + NativeInputEvent.getModifiersText(e.getModifiers()));
        KeyEvents keyEvent = getEventType(e);
        ApplicationController context = ApplicationController.getInstance();
        if (keyEvent == KeyEvents.COPY) {
            context.processCopyEvent();
        }
        if (keyEvent == KeyEvents.DECREASE_POSITION) {
            context.processDecreasePositionEvent();
        }
        if (keyEvent == KeyEvents.INCREASE_POSITION) {
            context.processIncreasePositionEvent();
        }
        if (e.getKeyCode() == NativeKeyEvent.VK_ESCAPE) {
            GlobalScreen.unregisterNativeHook();
        }
    }

    public void nativeKeyReleased(NativeKeyEvent e) {
        System.out.println("Key Released: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
    }

    public void nativeKeyTyped(NativeKeyEvent e) {
        System.out.println("Key Typed: " + e.getKeyText(e.getKeyCode()));
    }

    private KeyEvents getEventType(NativeKeyEvent e) {
        if ((e.getModifiers() & NativeInputEvent.CTRL_MASK) != 0
                && e.getKeyCode() == NativeKeyEvent.VK_C) {
            return KeyEvents.COPY;
        }
        if ((e.getModifiers() & NativeInputEvent.CTRL_MASK) != 0
                && (e.getModifiers() & NativeInputEvent.ALT_MASK) != 0
                && e.getKeyCode() == NativeKeyEvent.VK_DOWN) {
            return KeyEvents.DECREASE_POSITION;
        }
        if ((e.getModifiers() & NativeInputEvent.CTRL_MASK) != 0
                && (e.getModifiers() & NativeInputEvent.ALT_MASK) != 0
                && e.getKeyCode() == NativeKeyEvent.VK_UP) {
            return KeyEvents.INCREASE_POSITION;
        }
        return null;
    }
}
