package com.jclipboard.model;

/**
 * Created by Cinderella Kant on 20.04.14.
 */
public interface JClipboard<T> {
    public void addElement(T element);

    public T getElement();

    public T getElement(int position);

    public boolean reducePosition();

    public boolean increasePosition();

    public void setPosition(int position);

    public int getPosition();
}
