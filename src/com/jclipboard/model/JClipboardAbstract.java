package com.jclipboard.model;

/**
 * Created by Cinderella Kant on 03.05.14.
 */
public abstract class JClipboardAbstract<T> implements JClipboard<T> {
    private int position;

    public JClipboardAbstract() {
        position = -1;
    }

    @Override
    public abstract void addElement(T element);

    @Override
    public abstract T getElement();

    @Override
    public abstract T getElement(int position);

    @Override
    public boolean reducePosition() {
        if (position > 0) {
            position--;
            return true;
        }
        return false;
    }

    @Override
    public boolean increasePosition() {
        position++;
        return true;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getPosition() {
        return position;
    }
}
