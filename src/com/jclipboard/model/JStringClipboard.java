package com.jclipboard.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cinderella Kant on 20.04.14.
 */
public class JStringClipboard extends JClipboardAbstract<String> {
    private static volatile JStringClipboard clipboard;
    private List<String> buffer;

    private JStringClipboard() {
        buffer = new ArrayList<String>();
    }

    public static JStringClipboard getInstance() {
        if (clipboard == null) {
            synchronized (JStringClipboard.class) {
                clipboard = new JStringClipboard();
            }
        }
        return clipboard;
    }

    @Override
    public void addElement(String element) {
        if (element == null) {
            return;
        }
        if (!buffer.isEmpty()) {
            if (element.equals(buffer.get(getPosition()))) {
                return;
            }
            if (getPosition() != buffer.size() - 1) {
                buffer = buffer.subList(0, getPosition() + 1);
                setPosition(buffer.size() - 1);
            }
        }
        buffer.add(element);
        increasePosition();
    }

    @Override
    public String getElement() {
        return buffer.get(getPosition());
    }

    @Override
    public String getElement(int position) {
        return buffer.get(position);
    }

    @Override
    public boolean increasePosition() {
        if (getPosition() < buffer.size() - 1) {
            return super.increasePosition();
        }
        return false;
    }
}
